var endstate = {
    preload: function(){

    },
    create: function(){
        game.physics.startSystem(Phaser.Physics.ARCADE);
        game.renderer.renderSession.roundPixels = true;

        ceiling = game.add.sprite(0, 0, 'ceiling');
        leftWall = game.add.tileSprite(0, 0, 18, 400, 'wall');
        rightWall = game.add.tileSprite(382, 0, 18, 400, 'wall');
        inst = game.add.text(30, 300, '', style_small);
        var scoreLabel = game.add.text(500, game.height/2+30,'loading...' , { font: '25px Arial', fill: '#ffffff' });
        scoreLabel.anchor.setTo(0.5, 0.5);
    
        var database = firebase.database().ref('/scoreboard').orderByChild('nscore').limitToFirst(5);
        database.once('value', function(snapshot){
            var scoreText = '';
            snapshot.forEach(function(childSnapshot){
                scoreText += childSnapshot.val().name + '  B' + childSnapshot.val().score + '\n';
            });
            scoreLabel.text = scoreText;
        });

    
        this.cursor = game.input.keyboard.createCursorKeys();

        var player = prompt("Please enter your name", "name");

        if(player != null){
            var ref = firebase.database().ref("scoreboard");
                var data = {
                    name: player,
                    score: distance,
                    nscore: -distance
                }
            ref.push(data);
        }

        
    },

    update: function(){
        leftWall.tilePosition.y -= 1;
        rightWall.tilePosition.y -= 1;
        inst.setText('press ← or → to restart');

        if( this.cursor.left.isDown || this.cursor.right.isDown )
            game.state.start('load');
    }
};