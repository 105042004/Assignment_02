var playstate = {
    create: function() {

        /*create walls */
        ceiling = game.add.image(0, 0, 'ceiling');

        leftWall = game.add.tileSprite(0, 0, 18, 400, 'wall');
        game.physics.arcade.enable(leftWall);
        leftWall.body.immovable = true;

        rightWall = game.add.tileSprite(382, 0, 18, 400, 'wall');
        game.physics.arcade.enable(rightWall);
        rightWall.body.immovable = true;

        
        distance = 0;

        keyboard = game.input.keyboard.addKeys({
            'left': Phaser.Keyboard.LEFT,
            'right': Phaser.Keyboard.RIGHT,
        });

        createPlayer();
        createTextsBoard();
        initialPlaforms();

        BGM = game.add.audio('BGM');
        BGM.play();
        BGM.loop = true; 

        nailsound = game.add.audio('nails');
        trampsound = game.add.audio('tramp');
        fakesound = game.add.audio('fake');
        deadsound = game.add.audio('dead');
        conveyorsound = game.add.audio('conveyor');

    },

    update: function(){
        leftWall.tilePosition.y -= 1;
        rightWall.tilePosition.y -= 1;

        this.physics.arcade.collide(player, platforms, effect);
        this.physics.arcade.collide(player, [leftWall, rightWall]);
        checkTouchCeiling(player);
        checkGameOver();

        updatePlatforms();
        createPlatforms();
        updateTextsBoard();
        updatePlayer();
    
    }
};

function initialPlaforms(){
    
    var platform= game.add.sprite(150, 232, 'normal');
    game.physics.arcade.enable(platform);
    platform.body.immovable = true;
    platforms.push(platform);
    
    

    platform.body.checkCollision.down = false;
    platform.body.checkCollision.left = false;
    platform.body.checkCollision.right = false;
}



var lastTime = 0;
function createPlatforms () {
    if(game.time.now > lastTime + 600) {
        lastTime = game.time.now;
        createOnePlatform();
        distance += 1;
    }
}

function createOnePlatform () {

    var platform;
    var x = Math.random()*(400 - 96 - 40) + 20;
    var y = 400;
    var rand = Math.random() * 100;

    if(rand < 20) {
        platform = game.add.sprite(x, y, 'normal');
    } else if (rand < 40) {
        platform = game.add.sprite(x, y, 'nails');
        game.physics.arcade.enable(platform);
        platform.body.setSize(96, 15, 0, 15);
    } else if (rand < 50) {
        platform = game.add.sprite(x, y, 'conveyorLeft');
        platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
        platform.play('scroll');
    } else if (rand < 60) {
        platform = game.add.sprite(x, y, 'conveyorRight');
        platform.animations.add('scroll', [0, 1, 2, 3], 16, true);
        platform.play('scroll');
    } else if (rand < 80) {
        platform = game.add.sprite(x, y, 'trampoline');
        platform.animations.add('jump', [4, 5, 4, 3, 2, 1, 0, 1, 2, 3], 120);
        platform.frame = 3;
    } else {
        platform = game.add.sprite(x, y, 'fake');
        platform.animations.add('turn', [0, 1, 2, 3, 4, 5, 0], 14);
    }

    game.physics.arcade.enable(platform);
    platform.body.immovable = true;
    platforms.push(platform);

    platform.body.checkCollision.down = false;
    platform.body.checkCollision.left = false;
    platform.body.checkCollision.right = false;
}

function createPlayer() {
    player = game.add.sprite(180, 200, 'player');
    player.direction = 10;
    game.physics.arcade.enable(player);
    player.body.gravity.y = 500;
    player.animations.add('left', [0, 1, 2, 3], 8);
    player.animations.add('right', [9, 10, 11, 12], 8);
    player.animations.add('flyleft', [18, 19, 20, 21], 12);
    player.animations.add('flyright', [27, 28, 29, 30], 12);
    player.animations.add('fly', [36, 37, 38, 39], 12);
    player.life = 5;
    player.unbeatableTime = 0;
    player.touchOn = undefined;
}

function createTextsBoard () {
    
    text1 = game.add.text(405, 10, '', style);
    text2 = game.add.text(405, 55, '', style);
}

function updatePlayer () {
    if(keyboard.left.isDown) {
        player.body.velocity.x = -250;
    } else if(keyboard.right.isDown) {
        player.body.velocity.x = 250;
    } else {
        player.body.velocity.x = 0;
    }
    setPlayerAnimate(player);
}

function setPlayerAnimate(player) {
    var x = player.body.velocity.x;
    var y = player.body.velocity.y;

    if (x < 0 && y > 0) {
        player.animations.play('flyleft');
    }
    if (x > 0 && y > 0) {
        player.animations.play('flyright');
    }
    if (x < 0 && y == 0) {
        player.animations.play('left');
    }
    if (x > 0 && y == 0) {
        player.animations.play('right');
    }
    if (x == 0 && y != 0) {
        player.animations.play('fly');
    }
    if (x == 0 && y == 0) {
      player.frame = 8;
    }
}

function updatePlatforms () {
    for(var i=0; i<platforms.length; i++) {
        var platform = platforms[i];
        platform.body.position.y -= 2;
        if(platform.body.position.y <= -20) {
            platform.destroy();
            platforms.splice(i, 1);
        }
    }
}

function updateTextsBoard () {
    text1.setText('life:' + player.life + '/5');
    text2.setText('B' + distance);
}

function effect(player, platform) {
    if(platform.key == 'conveyorRight') {
        conveyorRightEffect(player, platform);
    }
    if(platform.key == 'conveyorLeft') {
        conveyorLeftEffect(player, platform);
    }
    if(platform.key == 'trampoline') {
        trampolineEffect(player, platform);
    }
    if(platform.key == 'nails') {
        nailsEffect(player, platform);
    }
    if(platform.key == 'normal') {
        basicEffect(player, platform);
    }
    if(platform.key == 'fake') {
        fakeEffect(player, platform);
    }
}

function conveyorRightEffect(player, platform) {
    player.body.x += 2;
    conveyorsound.play();
}

function conveyorLeftEffect(player, platform) {
    player.body.x -= 2;
    conveyorsound.play();
}

function trampolineEffect(player, platform) {
    platform.animations.play('jump');
    player.body.velocity.y = -280;
    trampsound.play();
}

function nailsEffect(player, platform) {
    if (player.touchOn !== platform) {
        player.life -= 1;
        player.touchOn = platform;
        nailsound.play();
        game.camera.flash(0xff0000, 100);
    }
}

function basicEffect(player, platform) {
    if (player.touchOn !== platform) {
        if(player.life < 5) {
            player.life += 1;
        }
        player.touchOn = platform;
    }
}

function fakeEffect(player, platform) {
    if(player.touchOn !== platform) {
        platform.animations.play('turn');
        setTimeout(function() {
            platform.body.checkCollision.up = false;
        }, 100);
        player.touchOn = platform;
        fakesound.play();
    }
}

function checkTouchCeiling(player) {
    if(player.body.y < 0) {
        if(player.body.velocity.y < 0) {
            player.body.velocity.y = 0;
        }
        if(game.time.now > player.unbeatableTime) {
            player.life -= 3;
            game.camera.flash(0xff0000, 100);
            nailsound.play();
            player.unbeatableTime = game.time.now + 2000;
        }
    }
}

function checkGameOver () {
    if(player.life <= 0 || player.body.y > 500) {
        deadsound.play();
        BGM.destroy();
        gameOver();
    }
}

function gameOver () {
    //this.ceiling.kill();
    platforms.forEach(function(s) {s.destroy()});
    platforms = [];
    game.state.start('end');
}



