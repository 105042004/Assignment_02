var loadstate = {
    preload: function(){
        game.load.image('ceiling', 'assets/ceiling.png');
        game.load.image('normal', 'assets/normal.png');
        game.load.image('wall', 'assets/wall.png');
        game.load.image('nails', 'assets/nails.png');

        game.load.spritesheet('player', 'assets/player.png', 32, 32);
        game.load.spritesheet('conveyorLeft', 'assets/conveyor_left.png', 96, 16);
        game.load.spritesheet('conveyorRight', 'assets/conveyor_right.png', 96, 16);
        game.load.spritesheet('fake', 'assets/fake.png', 97, 36);
        game.load.spritesheet('trampoline', 'assets/trampoline.png', 97, 22);

        game.load.audio('BGM','assets/sound/BGM.wav');
        game.load.audio('nails','assets/sound/nails.wav');
        game.load.audio('tramp','assets/sound/tramp.ogg');
        game.load.audio('fake','assets/sound/fake.mp3');
        game.load.audio('dead','assets/sound/dead.mp3');
        game.load.audio('conveyor','assets/sound/conveyor.mp3');
    },
    create: function(){
        game.physics.startSystem(Phaser.Physics.ARCADE);
        game.renderer.renderSession.roundPixels = true;

        ceiling = game.add.sprite(0, 0, 'ceiling');
        leftWall = game.add.tileSprite(0, 0, 18, 400, 'wall');
        rightWall = game.add.tileSprite(382, 0, 18, 400, 'wall');
    
        this.cursor = game.input.keyboard.createCursorKeys();
        logo = game.add.text(50, 100, '', style_big);
        game.add.tween(logo).to({angle:-2}, 500).to({angle:2}, 1000).to({angle:0}, 500).loop().start();
        
        inst = game.add.text(20, 300, '', style);
    },

    update: function(){
        leftWall.tilePosition.y -= 1;
        rightWall.tilePosition.y -= 1;
        logo.setText('NS-SHAFT');
        inst.setText('press ← or → to start');

        if( this.cursor.left.isDown || this.cursor.right.isDown )
            game.state.start('play');
    }

};