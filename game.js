// Initialize Phaser
var game = new Phaser.Game(600, 400, Phaser.AUTO, 'canvas');
var player;
var keyboard;
var style_small = {fill: '#ffaaaa', fontSize: '30px'};
var style = {fill: '#ffaaaa', fontSize: '35px'};
var style_big = {fill: '#ffaaaa', fontSize: '60px'};

var platforms = [];

var leftWall;
var rightWall;
var ceiling;

var text1;
var text2;
var logo;
var inst;

var replay = false;

var distance = 0;


// Add all the states
game.state.add('load', loadstate);
game.state.add('play', playstate);
game.state.add('end', endstate);

game.state.start('load');